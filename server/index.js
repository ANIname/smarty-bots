const cluster     = require('cluster');
const { cpus }    = require('os');
const { forEach } = require('../lib/enumeration');

forEach(cpus(), (cpu, index) => {
  let worker;

  switch (index) {
    default: {
      // Start HTTP(s) Server
      worker = 'server/server';
    }
  }

  cluster.setupMaster({ exec: worker });

  cluster.fork();
});
