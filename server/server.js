const http        = require('http');
const https       = require('https');
const url         = require('url');
const { extname } = require('path');
const config      = require('./config');
const MIME        = require('./mimeTypes');
const routes      = require('../lib/routes');
const staticFiles = require('../lib/staticFiles');
const template    = require('../lib/template');

/**
 * HTTP(s) Server Handler
 * @param {Object} req Request
 * @param {Object} res Response
 */
function serverHandler(req, res) {
  const path         = url.parse(req.url);
  const { pathname } = path;
  const isIndex      = pathname === '/';
  const isStatic     = pathname.startsWith('/static');

  if (isStatic) {
    sendStatic();
  }

  else {
    sendRoute();
  }

  function sendStatic() {
    if (staticFiles[pathname]) {
      res
        .writeHead(200, { 'Content-Type': MIME[extname(pathname)] || 'text/plain' })
        .end(staticFiles[pathname]);
    }

    else {
      res
        .writeHead(404, { 'Content-Type': 'text/plain' })
        .end('Static file not found!');
    }
  }

  function sendRoute() {
    if (routes[pathname]) {
      routes[pathname](path, (statusCode, layout) => {
        res.statusCode = statusCode;  // Set Status Code

        layout = template.check.auth(layout); // Set Auth Content

        res.end(layout);  // Set Auth Content
      });
    }

    else if (isIndex) {
      res
        .writeHead(501, { 'Content-Type': 'text/plain' })
        .end('No index handler!');
    }

    else {
      res
        .writeHead(301, { Location: './' })
        .end();
    }
  }
}

http
  .createServer(serverHandler)
  .listen(config.httpServer.port, () => {
    console.log('HTTP  Server Started & Listen Port:', config.httpServer.port);
  });

https
  .createServer(config.httpsServer, serverHandler)
  .listen(config.httpsServer.port, () => {
    console.log('HTTPS Server Started & Listen Port:', config.httpsServer.port);
  });
