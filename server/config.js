const fs     = require('fs');
const terser = require('terser');

module.exports = {
  minify: {
    collapseBooleanAttributes   : true,
    collapseInlineTagWhitespace : true,
    collapseWhitespace          : true,
    minifyCSS                   : true,
    minifyJS                    : text => terser.minify(text).code,
    minifyURLs                  : true,
    removeAttributeQuotes       : true,
    removeComments              : true,
    removeEmptyAttributes       : true,
  },

  httpServer: {
    port: 8080,
  },

  httpsServer: {
    cert : fs.readFileSync('./https/cert.pem'),
    key  : fs.readFileSync('./https/key.pem'),
    port : 8081,
  },
};
