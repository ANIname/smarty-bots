/**
 * Executes a provided function once for each array element
 * @param {object} array
 * @param {function} callback Function to execute on each element, taking three arguments
 */
exports.forEach = (array, callback) => {
  if (typeof array !== 'object') return console.error('forEach Error: array must be an object!');

  let counter = array.length - 1;
  for (counter; counter >= 0; counter--) {
    callback(array[counter], counter);
  }
};
