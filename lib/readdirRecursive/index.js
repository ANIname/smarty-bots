const async = require('async');
const fs    = require('fs');

const readdirRecursive = {
  /**
   * Asynchronous Get All Files In Directory & Subdirectories
   * @param {String} dir A path to a directory
   * @param {Function} callback
   */
  async: (dir, callback) => {
    if (!dir || typeof dir !== 'string') return console.error('No directory specified');

    fs.readdir(dir, (err, items) => {
      if (err) return console.error('Get Files Error:', err);

      async.each(items, checkItem);
    });

    function checkItem(item) {
      const path = `${dir}/${item}`;

      fs.stat(path, (err, stat) => {
        if (err) return console.error('Check Item Error:', err);

        if (stat.isDirectory()) return readdirRecursive.async(path, callback);

        callback(path);
      });
    }
  },

  /**
   * Synchronous Get All Files In Directory & Subdirectories
   * @param {String} dir A path to a directory
   * @param {Function} callback
   */
  sync: (dir, callback) => {
    if (!dir || typeof dir !== 'string') return console.error('No directory specified');

    const items = fs.readdirSync(dir);

    async.each(items, (item) => {
      const path        = `${dir}/${item}`;
      const isDirectory = fs.statSync(path).isDirectory();

      if (isDirectory) return readdirRecursive.sync(path, callback);

      callback(path);
    });
  },
};

module.exports = readdirRecursive;
