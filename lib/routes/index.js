const readdirRecursive = require('../readdirRecursive');

const routes = {};

// Get All Routes From './routes' Dir & PUT It Into RAM.
readdirRecursive.async('./routes', (path) => {
  const isIndex = path.endsWith('index.js');

  // Change Path
  // Ex: FROM ./routes/blog/index.js TO /blog/index
  let routePath = path.slice('./routes'.length, path.length - '.js'.length);

  if (isIndex) {
    // Change Route Path
    // EX: FROM /blog/index TO /blog/
    routePath = routePath.slice(null, routePath.length - 'index'.length);
  }

  routes[routePath] = require(`../.${path}`);
});

module.exports = routes;
