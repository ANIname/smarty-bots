const async             = require('async');
const { StringDecoder } = require('string_decoder');
const { minify }        = require('html-minifier');
const staticFiles       = require('../staticFiles');
const config            = require('../../server/config');

const decoder = new StringDecoder('utf8');

/**
 * Get Static Content
 * @param   {String} pageName Name HTML Page Into static/pages Folded
 * @returns {String} Minify Template Code
 */
exports.get = (pageName) => {
  const styles = staticFiles[`/static/css/${pageName}.css`];

  let template = decoder.write(staticFiles['/static/template.html']);
  let page     = decoder.write(staticFiles[`/static/pages/${pageName}.html`]);

  // PUT Components From '/static/components/' Folder To Template
  async.eachOf(staticFiles, (data, key) => {
    const isComponent = key.startsWith('/static/components/');

    if (isComponent) {
      const name = key.slice('/static/components/'.length, key.length - '.html'.length);

      page = page.replace(`<${name}></${name}>`, data);
    }
  });

  if (styles) {
    // PUT Custom Page Styles To Template
    template = template.replace('</head>', `<style>${styles}</style></head>`);
  }

  // Add Page Code To Template
  template = template.replace('<main></main>', page);

  // Minify Result
  return minify(template, config.minify);
};

exports.check = {
  /**
   * @param {String} layout
   * @param {Boolean} isAuth User Is Authorized?
   * @returns {String} Modifed Layout
   */
  auth: (layout, isAuth) => {
    if (isAuth) {
      layout = layout
        .replace(/<if-not-auth>([\s\S]*?)<\/if-not-auth>/g, '')
        .replace(/<if-auth>|<\/if-auth>/g, '');
    }

    else {
      layout = layout
        .replace(/<if-auth>([\s\S]*?)<\/if-auth>/g, '')
        .replace(/<if-not-auth>|<\/if-not-auth>/g, '');
    }

    return layout;
  },
};
