const fs = require('fs');
const readdirRecursive = require('../readdirRecursive');

const staticFiles = {};

/**
 * Get All Static Files From './static' Dir & PUT It Into RAM.
 *
 * We Using Sync Method Bacause We Need Load Static Files Before Server Started.
 * But If Static File Extension Is Not: ".html", ".css", ".js" - We Load It Asynchronously!
 */
readdirRecursive.sync('./static', (path) => {
  const isMainFile = path.endsWith('html') || path.endsWith('css') || path.endsWith('js');

  if (isMainFile) {
    staticFiles[path.slice(1)] = fs.readFileSync(path);
  }

  else {
    fs.readFile(path, (err, data) => {
      if (err) return console.error('Read Static File Error:', err);

      staticFiles[path.slice(1)] = data;
    });
  }
});

module.exports = staticFiles;
