/**
 * Work With Each Cell Of The Array
 * @param {Array} array
 * @param {Function} callback
 */
function forEach(array, callback) {
  let counter = array.length - 1;

  for (counter; counter >= 0; counter--) {
    callback(array[counter], counter);
  }
}
