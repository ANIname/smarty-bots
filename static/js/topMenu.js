const topMenu      = document.querySelector('#topMenu');
const topMenuItems = topMenu.querySelectorAll('a');

forEach(topMenuItems, (item) => {
  if (item.href.endsWith(document.URL)) {
    item.className = 'current';
  }
});
