const template = require('../lib/template');

const layot = template.get('index');

module.exports = (path, callback) => {
  callback(200, layot);
};
